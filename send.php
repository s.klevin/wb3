<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

  if (!empty($_GET['save'])) {
    print('<div class="alert alert-success col-sm-12 col-md-3 mt-3 mx-auto">Спасибо, результаты сохранены!</div>');
  }
  include('index.php');
  exit();
}
$errors = FALSE;

if (empty($_POST['name'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}

if (empty($_POST['email'])) {
  print('Заполните почту.<br/>');
  $errors = TRUE;
}

if (empty($_POST['powers'])) {
  print('Заполните способности.<br/>');
  $errors = TRUE;
}

if (empty($_POST['bio'])) {
  print('Заполните биографию.<br/>');
  $errors = TRUE;
}

if (empty($_POST['check'])) {
  print('Нельзя отправить форму без согласия с контрактом.<br/>');
  $errors = TRUE;
}

if ($errors) {
  exit();
}
$user = 'u16432';
$pass = '2294342';
$db = new PDO('mysql:host=localhost;dbname=u16432', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $stmt = $db->prepare("INSERT INTO wb3 SET name = ?, email = ?, age = ?, sex = ?, limbs = ?, powers = ?, bio = ?");
  $stmt -> execute(array(
    $_POST['name'],
    $_POST['email'],
    $_POST['age'],
    $_POST['sex'],
    $_POST['limbs'],
    implode(', ', $_POST['powers']),
    $_POST['bio']
  ));
}
catch(PDOException $e){
  print('Ошибка: ' . $e->getMessage());
  exit();
}

header('Location: ?save=1');